public class Rupee {

    private final int value;

    public Rupee(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Rupee )){
            return true;
        }
        Rupee rupee = (Rupee)o;

        return value == rupee.value;
    }
}
