import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;

public class RupeeTest {

    @Test
    void oneTenRupeeNoteNotSameAsAnotherTenRupeeNote() {
        Rupee tenRupee1 = new Rupee(10);
        Rupee tenRupee2 = new Rupee(10);

        assertNotSame(tenRupee1, tenRupee2);
    }

    @Test
    void valueOfTwoTenRupeeNotesAreSame() {
        Rupee tenRupee1 = new Rupee(10);
        Rupee tenRupee2 = new Rupee(10);

        assertEquals(true, tenRupee1.equals(tenRupee2));
    }

    @Test
    void tenRupeesNoteIsNotEqualToFiveRupeesNote() {
        Rupee fiveRupee = new Rupee(5);
        Rupee tenRupee = new Rupee(10);

        assertNotEquals(fiveRupee,tenRupee);
    }

    @Test
    void tenRupeeIsNotNull(){
        Rupee tenRupee = new Rupee(10);

        assertNotNull(tenRupee);
    }

    @Test
    void tenRupeesIsNotADifferentObject() {
        Rupee tenRupee = new Rupee(10);
        Integer integer = 10;

        assertTrue(tenRupee.equals(integer));
    }
}
